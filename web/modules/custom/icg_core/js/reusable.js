const $ = jQuery;

$(document).bind('change', '.field--name-field-content-display select', function(e) {
  let value = $(e.target).val();
  if (value === 'collection') {
    $(e.target).parents('.vertical-tabs').find('#edit-group-collection').show()
  }
});