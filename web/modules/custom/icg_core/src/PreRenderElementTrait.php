<?php

namespace Drupal\icg_core;

/**
 * Using this trait will add preRenderElement() method to the class.
 *
 * If the class is capable of injecting services from the container, it should
 * inject the 'controller_resolver' service and assign it to
 * $this->controllerResolver.
 */
trait PreRenderElementTrait {

  /**
   * The controller resolver.
   *
   * @var \Drupal\Core\Controller\ControllerResolverInterface
   */
  protected $controllerResolver;

  /**
   * Call any pre_render hooks on element.
   *
   * This is useful for breaking apart an entity render element to
   * access the child render elements.
   *
   * @param array $element
   *   Render element.
   */
  public function preRenderElement(array $element) {
    if (isset($element['#pre_render'])) {
      foreach ($element['#pre_render'] as $callable) {
        if (is_string($callable) && strpos($callable, '::') === FALSE) {
          $callable = $this->getControllerResolver()->getControllerFromDefinition($callable);
        }
        $element = call_user_func($callable, $element);
      }
    }

    return $element;
  }

  /**
   * Gets the controller resolver service.
   *
   * @return \Drupal\Core\Controller\ControllerResolverInterface
   *   The controller resolver service.
   */
  protected function getControllerResolver() {
    if (!$this->controllerResolver) {
      $this->controllerResolver = \Drupal::service('controller_resolver');
    }

    return $this->controllerResolver;
  }

  /**
   * Sets the controller resolver service to use.
   *
   * @param \Drupal\Core\Controller\ControllerResolverInterface $controller_resolver
   *   The controller resolver service.
   *
   * @return $this
   */
  public function setControllerResolver(ControllerResolverInterface $controller_resolver) {
    $this->controllerResolver = $controller_resolver;

    return $this;
  }

}
