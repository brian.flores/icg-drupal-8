<?php

namespace Drupal\icg_core\Component;

use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\icg_core\CacheableMetadataTrait;
use Drupal\icg_core\PreRenderElementTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for component rendering.
 */
abstract class BaseComponent {

  use StringTranslationTrait;
  use CacheableMetadataTrait;
  use PreRenderElementTrait;

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public $entityTypeManager;

  /**
   * The string translation manager.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * The controller resolver.
   *
   * @var \Drupal\Core\Controller\ControllerResolverInterface
   */
  protected $controllerResolver;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a ComponentBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   * @param \Drupal\Core\StringTranslation\TranslationManager $string_translation
   *   Custom datetime formatter.
   * @param \Drupal\Core\Controller\ControllerResolverInterface $controller_resolver
   *   The controller resolver.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationManager $string_translation, ControllerResolverInterface $controller_resolver, RendererInterface $renderer, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
    $this->controllerResolver = $controller_resolver;
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * Creates a ComponentBase class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('string_translation'),
      $container->get('controller_resolver'),
      $container->get('renderer'),
      $container->get('date.formatter')
    );
  }

  /**
   * Renders HTML given a structured array tree.
   *
   * @param array $elements
   *   The structured array describing the data to be rendered.
   * @param bool $is_root_call
   *   (Internal use only.) Whether this is a recursive call or not. See
   *   ::renderRoot().
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The rendered HTML.
   *
   * @see \Drupal\Core\Render\Renderer::render()
   */
  public function render(array &$elements, $is_root_call = FALSE) {
    return $this->renderer->render($elements, $is_root_call);
  }

  /**
   * Check if render array is empty.
   *
   * @param mixed $element
   *   The structured render array.
   */
  public function isEmpty($element) {
    return empty($element) || (is_array($element) && count($element) == 2 && isset($element['#cache']) && isset($element['#weight']));
  }

}
