<?php

namespace Drupal\icg_core;

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Using this trait will add metadata related helper methods to the class.
 */
trait CacheableMetadataTrait {

  /**
   * Bubble nested metadata.
   *
   * Gets the cacheable metadata in the content part of a render array and
   * apply to the cache tags of the top level variable in the preprocess
   * function. This is useful when you're pulling out raw values and not
   * rendering the elements within the content portion of preprocess function.
   *
   * @param array $element
   *   A render array.
   */
  public static function bubbleNestedMetadata(array &$element) {
    $metadata = BubbleableMetadata::createFromRenderArray($element);

    foreach ($element as $key => $child) {
      if (strpos($key, '#') === 0) {
        continue;
      }

      // Skip elements on node, since it's redundant with content.
      if ($key == 'elements' && !empty($element['theme_hook_original']) && $element['theme_hook_original'] == 'node') {
        continue;
      }

      if (is_array($child)) {
        static::bubbleNestedMetadata($child);
        $child_metadata = BubbleableMetadata::createFromRenderArray($child);
        $metadata = $metadata->merge($child_metadata);
      }
    }

    $metadata->applyTo($element);
  }

  /**
   * Gets the cacheable metadata from $other and apply it to $element.
   *
   * @param array $element
   *   A render array.
   * @param array $other
   *   A render array.
   */
  public function mergeMetadataTo(array &$element, array $other) {
    $metadata = BubbleableMetadata::createFromRenderArray($other);
    $metadata->applyTo($element);
  }

  /**
   * Remove metadata from render element.
   *
   * This is useful if you plan iterate through the
   * element in the twig template.
   *
   * @param array &$element
   *   A render element.
   */
  public static function removeMetadata(array &$element) {
    unset($element['#cache']);
    unset($element['#weight']);
  }

}
