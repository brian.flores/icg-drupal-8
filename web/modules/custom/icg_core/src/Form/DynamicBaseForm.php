<?php

namespace Drupal\icg_core\Form;

/**
 * Base form for Content Grid, List Sorter, and Content Promo Dynamic.
 */
class DynamicBaseForm {

  /**
   * Get element value.
   */
  public function getValue($element) {
    $value = NULL;
    if (!empty($element['#value'])) {
      $value = $element['#value'];
    }
    elseif (!empty($element['#default_value'])) {
      $value = reset($element['#default_value']);
    }

    return $value;
  }

  /**
   * Build element ID.
   */
  protected function buildElementId(&$element_container) {
    if (!empty($element_container['widget']['#parents'])) {
      $id = $this->getElementId($element_container['widget']);
      $element_container['#prefix'] = "<div id=\"{$id}\">";
      $element_container['#suffix'] = "</div>";

      return $id;
    }

    return FALSE;
  }

  /**
   * Build element ID.
   */
  protected function buildFormId(&$form_container) {
    if (!empty($form_container['#parents'])) {
      $id = $this->getElementId($form_container);
      $form_container['#prefix'] = "<div id=\"{$id}\">";
      $form_container['#suffix'] = "</div>";

      return $id;
    }

    return FALSE;
  }

  /**
   * Get element ID.
   */
  protected function getElementId($element) {
    if (!empty($element['#parents'])) {
      $id = str_replace('_', '-', implode('--', $element['#parents']));
      return $id;
    }

    return FALSE;
  }

  /**
   * Set element callback.
   */
  protected function setElementCallback(&$element, $callback) {
    if (!empty($element)) {
      $element['#ajax'] = ['callback' => $callback];
      return $callback;
    }

    return FALSE;
  }

  /**
   * Set element options.
   */
  protected function setElementOptions(&$element, $options) {
    if (!empty($element)) {
      $element['#options'] = $options;
      return $options;
    }

    return [];
  }

  /**
   * Set element validation.
   */
  protected function setElementValidation(&$element, $validation_methods) {
    if (!empty($element)) {
      $element['#element_validate'][] = $validation_methods;
      return $validation_methods;
    }

    return FALSE;
  }

  /**
   * Get element name.
   */
  public function getElementName($element) {
    $element_parents = $element['widget']['#parents'];
    $element_name = $element_parents[0] . '[' . implode('][', array_slice($element_parents, 1)) . ']';
    return $element_name;
  }

}
