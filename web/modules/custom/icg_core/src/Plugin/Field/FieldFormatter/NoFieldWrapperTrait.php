<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Adds function viewWithoutFieldWrapper to classes that extend FormatterBase.
 *
 * Will return rendered elements without the field wrapper.
 */
trait NoFieldWrapperTrait {

  /**
   * {@inheritdoc}
   */
  public function viewWithoutFieldWrapper(FieldItemListInterface $items, $langcode = NULL) {

    // Default the language to the current content language.
    if (empty($langcode)) {
      $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    $elements = $this->viewElements($items, $langcode);

    $cardinality = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getCardinality();

    // Return single element, or an array of elements,
    // rather than the elements nested in the field wrapper.
    if ($cardinality === 1 && !empty($elements[0])) {
      return $elements[0];
    }
    else {
      return $elements;
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    return $this->viewWithoutFieldWrapper($items, $langcode);
  }

}
