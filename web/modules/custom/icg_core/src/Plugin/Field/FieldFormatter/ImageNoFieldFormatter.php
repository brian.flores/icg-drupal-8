<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'image' formatter.
 *
 * @FieldFormatter(
 *   id = "image_no_field",
 *   label = @Translation("Image, no field wrapper"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ImageNoFieldFormatter extends ImageFormatter {

  use NoFieldWrapperTrait;

}
