<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceIdFormatter;

/**
 * Plugin implementation of the 'entity reference ID' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_id_no_field",
 *   label = @Translation("Entity ID no field wrapper"),
 *   description = @Translation("Display the ID of the referenced entities."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceIdNoFieldFormatter extends EntityReferenceIdFormatter {

  use NoFieldWrapperTrait;

}
