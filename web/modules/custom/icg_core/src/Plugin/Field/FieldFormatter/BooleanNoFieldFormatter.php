<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\BooleanFormatter;

/**
 * Plugin implementation of the 'boolean' formatter.
 *
 * @FieldFormatter(
 *   id = "boolean_no_field",
 *   label = @Translation("Boolean, no field wrapper"),
 *   field_types = {
 *     "boolean",
 *   }
 * )
 */
class BooleanNoFieldFormatter extends BooleanFormatter {

  use NoFieldWrapperTrait;

}
