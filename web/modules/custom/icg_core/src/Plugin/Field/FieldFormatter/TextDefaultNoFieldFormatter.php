<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * Plugin implementation of the 'text_default_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "text_default_no_field",
 *   label = @Translation("Default, no field wrapper"),
 *   field_types = {
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   }
 * )
 */
class TextDefaultNoFieldFormatter extends TextDefaultFormatter {

  use NoFieldWrapperTrait;

}
