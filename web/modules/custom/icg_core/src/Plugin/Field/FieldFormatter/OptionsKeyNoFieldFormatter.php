<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsKeyFormatter;

/**
 * Plugin implementation of the 'list_key_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "list_key_no_field",
 *   label = @Translation("Key no field wrapper"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class OptionsKeyNoFieldFormatter extends OptionsKeyFormatter {

  use NoFieldWrapperTrait;

}
