<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_label_no_field",
 *   label = @Translation("Label, no field wrapper"),
 *   description = @Translation("Display the label of the referenced entities."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceLabelNoFieldFormatter extends EntityReferenceLabelFormatter {

  use NoFieldWrapperTrait;

}
