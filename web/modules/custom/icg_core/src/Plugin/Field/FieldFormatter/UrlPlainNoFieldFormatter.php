<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\file\Plugin\Field\FieldFormatter\UrlPlainFormatter;

/**
 * Plugin implementation of the 'file_url_plain_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "file_url_plain_no_field",
 *   label = @Translation("URL to file, no field wrapper"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class UrlPlainNoFieldFormatter extends UrlPlainFormatter {

  use NoFieldWrapperTrait;

}
