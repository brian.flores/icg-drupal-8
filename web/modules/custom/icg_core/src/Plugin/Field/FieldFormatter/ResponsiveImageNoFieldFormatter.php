<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;

/**
 * Plugin for responsive image formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_image_no_field",
 *   label = @Translation("Responsive image, no field wrapper"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class ResponsiveImageNoFieldFormatter extends ResponsiveImageFormatter {

  use NoFieldWrapperTrait;

}
