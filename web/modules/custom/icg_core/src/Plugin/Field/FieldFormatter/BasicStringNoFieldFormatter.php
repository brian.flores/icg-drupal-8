<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\BasicStringFormatter;

/**
 * Plugin implementation of the 'basic_string' formatter.
 *
 * @FieldFormatter(
 *   id = "basic_string_no_field",
 *   label = @Translation("Plain text, no field wrapper"),
 *   field_types = {
 *     "string_long",
 *     "email"
 *   }
 * )
 */
class BasicStringNoFieldFormatter extends BasicStringFormatter {

  use NoFieldWrapperTrait;

}
