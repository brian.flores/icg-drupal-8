<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\geolocation\Plugin\Field\FieldFormatter\GeolocationLatlngFormatter;

/**
 * Plugin implementation of the 'geolocation_latlng_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "geolocation_latlng_no_field",
 *   label = @Translation("Geolocation Lat/Lng, no field wrapper"),
 *   field_types = {
 *     "geolocation"
 *   }
 * )
 */
class GeolocationLatlngNoFieldFormatter extends GeolocationLatlngFormatter {

  use NoFieldWrapperTrait;

}
