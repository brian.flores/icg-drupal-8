<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeCustomFormatter;

/**
 * Plugin implementation of the 'Custom' formatter for 'datetime' fields.
 *
 * @FieldFormatter(
 *   id = "datetime_custom_no_field",
 *   label = @Translation("Custom, no field wrapper"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateTimeCustomNoFieldFormatter extends DateTimeCustomFormatter {

  use NoFieldWrapperTrait;

}
