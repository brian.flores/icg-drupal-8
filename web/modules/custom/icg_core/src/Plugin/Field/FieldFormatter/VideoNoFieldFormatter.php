<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\video_embed_field\Plugin\Field\FieldFormatter\Video;

/**
 * Plugin implementation of the 'video_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "video_embed_field_video_no_field",
 *   label = @Translation("Video, no field wrapper"),
 *   field_types = {
 *     "video_embed_field"
 *   }
 * )
 */
class VideoNoFieldFormatter extends Video {

  use NoFieldWrapperTrait;

}
