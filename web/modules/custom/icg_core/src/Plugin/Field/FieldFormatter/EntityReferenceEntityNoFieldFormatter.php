<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\icg_core\PreRenderElementTrait;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_view_no_field",
 *   label = @Translation("Rendered entity, no field wrapper"),
 *   description = @Translation("Display the referenced entities without a field wrapper."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceEntityNoFieldFormatter extends EntityReferenceEntityFormatter {

  use NoFieldWrapperTrait;
  use PreRenderElementTrait;

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements = parent::settingsForm($form, $form_state);

    $elements['pre_render'] = [
      '#type' => 'checkbox',
      '#title' => t('Pre-render to make child fields available in this template.'),
      '#default_value' => $this->getSetting('pre_render'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'pre_render' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = parent::settingsSummary();

    if ($this->getSetting('pre_render')) {
      $summary[] = $this->t('Pre-rendering to make child fields available in this template.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    if ($this->getSetting('pre_render') === "1") {
      foreach ($elements as &$element) {
        $element = $this->preRenderElement($element);
      }
    }

    return $elements;
  }

}
