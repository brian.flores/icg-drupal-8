<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileVideoFormatter;

/**
 * Plugin implementation of the 'file_video_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "file_video_no_field",
 *   label = @Translation("Video, no field wrapper"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileVideoNoFieldFormatter extends FileVideoFormatter {

  use NoFieldWrapperTrait;

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = $this->viewWithoutFieldWrapper($items, $langcode);

    $cardinality = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getCardinality();

    $plugin_id = 'file_video';
    if ($cardinality === 1 && isset($elements['#theme'])) {
      $elements['#theme'] = $plugin_id;
    }
    else {
      foreach ($elements as &$element) {
        $element['#theme'] = $plugin_id;
      }
    }

    return $elements;
  }

}
