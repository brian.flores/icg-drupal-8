<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'list_default_no_field' formatter.
 *
 * @FieldFormatter(
 *   id = "list_default_no_field",
 *   label = @Translation("Default no field wrapper"),
 *   field_types = {
 *     "list_integer",
 *     "list_float",
 *     "list_string",
 *   }
 * )
 */
class OptionsDefaultNoFieldFormatter extends OptionsDefaultFormatter {

  use NoFieldWrapperTrait;

}
