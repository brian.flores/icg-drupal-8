<?php

namespace Drupal\icg_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkSeparateFormatter;

/**
 * Plugin implementation of the 'link_separate' formatter.
 *
 * @FieldFormatter(
 *   id = "link_raw_no_field",
 *   label = @Translation("Link values as raw data, no field wrapper"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkRawNoFieldFormatter extends LinkSeparateFormatter {

  use NoFieldWrapperTrait;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);
    foreach ($elements as $key => $element) {

      /** @var \Drupal\Core\Url $url */
      $url = $element['#url'];
      $elements[$key] = [
        'title' => [
          '#markup' => $element['#title'],
        ],
        'url' => [
          '#markup' => $url->toString(),
        ],
      ];
    }
    return $elements;
  }

}
