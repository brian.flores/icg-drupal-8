<?php

namespace Drupal\paragraph_group\Plugin\Field\FieldWidget;

use Drupal\paragraphs_enhancements\Plugin\Field\FieldWidget\ParagraphsEnhancementsWidget as ParagraphsEnhancementsWidgetBase;

/**
 * Plugin implementation of the 'paragraphs_enhancements' paragraphs widget.
 *
 * @FieldWidget(
 *   id = "paragraphs_enhancements_modal",
 *   label = @Translation("Paragraphs Enhanced (Modal)"),
 *   description = @Translation("Extends Paragraphs EXPERIMENTAL to add  optgroup functionality."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class ParagraphsEnhancementsWidget extends ParagraphsEnhancementsWidgetBase {

  /**
   * Returns the available paragraphs type.
   *
   * @return array
   *   Available paragraphs types.
   */
  protected function getAccessibleOptions() {
    // This function is called twice for some reason. Do not remove!
    // @TODO: investigate why this is and what we can do about it.
    if (!empty($this->accessOptions)) {
      return $this->accessOptions;
    }

    // Get accessible options from parent widget.
    $this->accessOptions = parent::getAccessibleOptions();

    // If the widget is not using 'Select' for 'Add mode', do nothing.
    if (!in_array($this->getSetting('add_mode'), ['select', 'modal'])) {
      return $this->accessOptions;
    }

    // Get general optgroup config.
    $paragraph_optgroups = $this->getEntityTypeManager()
      ->getStorage('paragraph_optgroup')
      ->getQuery()
      ->sort('weight', 'ASC')
      ->execute();

    // Get this field's accessible paragraphs.
    $unordered_paragraphs = $this->accessOptions;

    $ordered_groups = [];
    foreach ($paragraph_optgroups as $optgroup) {
      $optgroup = $this->getEntityTypeManager()->getStorage('paragraph_optgroup')->load($optgroup);

      foreach ($unordered_paragraphs as $machine_name => $label) {
        if (in_array($machine_name, $optgroup->get('paragraphs'))) {
          $ordered_groups[$optgroup->label()][$machine_name] = $label;
          unset($unordered_paragraphs[$machine_name]);
        }
      }
    }

    // Sort option values if alphabetical sort if configured.
    if (!empty($this->getSetting('alphabetical_sort'))) {
      // Sort the groups one by one.
      // We still want to keep the groups themselves ordered as per the config.
      foreach ($ordered_groups as $group_name => $group) {
        natcasesort($ordered_groups[$group_name]);
      }
      // Sort the non grouped options.
      natcasesort($unordered_paragraphs);
    }

    // Merge grouped and non grouped options, grouped options first.
    return $this->accessOptions = $ordered_groups + ['Others' => $unordered_paragraphs];
  }

  /**
   * Builds dropdown button for adding new paragraph.
   *
   * @return array
   *   The form element array.
   */
  protected function buildButtonsAddMode() {
    $group_options = $this->getAccessibleOptions();
    $add_mode = $this->getSetting('add_mode');
    $paragraphs_type_storage = \Drupal::entityTypeManager()->getStorage('paragraphs_type');
    foreach ($group_options as $group_label => $options) {
      foreach ($options as $machine_name => $label) {
        $button_key = 'add_more_button_' . $machine_name;
        $add_more_elements[$group_label][$button_key] = $this->expandButton([
          '#type' => 'submit',
          '#name' => $this->fieldIdPrefix . '_' . $machine_name . '_add_more',
          '#value' => $add_mode == 'modal' ? $label : $this->t('Add @type', ['@type' => $label]),
          '#attributes' => ['class' => ['field-add-more-submit', 'paragraphs-add-wrapper']],
          '#limit_validation_errors' => [
            array_merge($this->fieldParents, [$this->fieldDefinition->getName(), 'add_more']),
          ],
          '#submit' => [
            [get_class($this), 'addMoreSubmit'],
          ],
          '#ajax' => [
            'callback' => [get_class($this), 'addMoreAjax'],
            'wrapper' => $this->fieldWrapperId,
          ],
          '#bundle_machine_name' => $machine_name,
        ]);

        if ($add_mode === 'modal' && $icon_url = $paragraphs_type_storage->load($machine_name)->getIconUrl()) {
          $add_more_elements[$button_key]['#attributes']['style'] = 'background-image: url(' . $icon_url . ');';
        }
      }
    }

    // Determine if buttons should be rendered as dropbuttons.
    if (count($options) > 1 && $add_mode == 'dropdown') {
      $add_more_elements = $this->buildDropbutton($add_more_elements);
      $add_more_elements['#suffix'] = $this->t('to %type', ['%type' => $this->fieldDefinition->getLabel()]);
    }
    elseif ($add_mode == 'modal') {
      $this->buildModalAddForm($add_more_elements);
      $add_more_elements['add_modal_form_area']['#suffix'] = '<span class="paragraphs-add-suffix">' . $this->t('to %type', ['%type' => $this->fieldDefinition->getLabel()]) . '</span>';
    }
    $add_more_elements['#weight'] = 1;

    return $add_more_elements;
  }

}
