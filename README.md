# Codebase for ICG Drupal 8

## System Requirements

- Composer
- Lando (v3.0.0-aft.1)
  [DMG location](https://github.com/lando/lando/releases)

## Installation Procedure

1. `lando start`
2. `lando site-install` to freshly install Drupal 8
3. `lando site-deploy` to deploy with the configs only.

## Export command

- `lando site-export`

## How to update Lando

1. `lando stop`
2. Download and install the latest Lando release
3. `lando rebuild`

## Lando SSH Example

```
# SSH into the appserver
lando ssh -s appserver

```

## Front End Setup

1. Open `web/themes/custom/icg_theme` in terminal.
3. Run `lando node-install` to install npm modules
4. Run `lando node-compile` to compile npm
5. Open the site with Lando's generated URL (e.g. https://localhost:32773/themes/custom/icg_theme/styleguide/)
